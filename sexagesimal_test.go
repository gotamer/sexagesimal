package sexagesimal

import (
	"testing"
)

func TestSeq(t *testing.T) {
	s := New()
	for i := 0; i < 219661; i++ {
		key := s.Next()
		if key == "00" {
			if i != 60 {
				t.Errorf("Should be 60 at key: %v but is %v", key, i)
			}
		}
		if key == "zz" {
			if i != 3659 {
				t.Errorf("Should be 3659 at key: %v but is %v", key, i)
			}
		}
		if key == "000" {
			if i != 3660 {
				t.Errorf("Should be 3660 at key: %v but is %v", key, i)
			}
		}
		if key == "0000" {
			if i != 219660 {
				t.Errorf("Should be 219660 at key: %v but is %v", key, i)
			}
		}
	}
}
