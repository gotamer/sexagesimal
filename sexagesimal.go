// Sexagesimal (base 60) is a numeral system with sixty as its base.
// "bitbucket.org/gotamer/sexagesimal"
package sexagesimal

import (
	"sync"
)

var Cunei = [60]string{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "r", "s", "t", "u", "v", "w", "x", "y", "z"}

type Sexagesimal struct {
	sync.Mutex
	next []string
}

func New() (s Sexagesimal) {
	s.next = append(s.next, Cunei[0])
	return
}

func (s *Sexagesimal) Next() (key string) {
	s.Lock()
	defer s.Unlock()
	for _, v := range s.next {
		key = key + string(v)
	}
	s.next = s.increment(s.next)
	return
}

func (s *Sexagesimal) increment(current []string) (next []string) {
	n := len(current)
	if n == 0 {
		next = append(current, Cunei[0])
	} else {
		last := current[n-1]
		prev := current[0 : n-1]
		if ni, ok := s.nextItem(last); ok {
			next = append(prev, ni)
		} else {
			current = s.increment(prev)
			next = append(current, Cunei[0])
		}
	}
	return
}

func (s *Sexagesimal) nextItem(current string) (next string, ok bool) {
	currentId := s.id(current)
	if currentId < len(Cunei)-1 {
		next = Cunei[currentId+1]
		ok = true
	}
	return
}

// Returns the id of the byte, or -1 if not found
func (s *Sexagesimal) id(key string) (id int) {
	id = -1
	for k, v := range Cunei {
		if v == key {
			id = k
			break
		}
	}
	return
}
